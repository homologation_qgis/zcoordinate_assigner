# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Z Coordinate Assigner
qgisMinimumVersion=2.0
description=This plugin assigns z coordinates to vector files
version=0.1
author=Angus Carr
email=angus.carr@gmail.com

about=Assigns Z Coordinate to a vector source. Z coordinates assigned from local or web sources.

tracker=https://gitlab.com/homologation_qgis/zcoordinate_assigner/issues
repository=https://anguscarr@gitlab.com/homologation_qgis/zcoordinate_assigner.git
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=elevation 

homepage=https://gitlab.com/homologation_qgis/zcoordinate_assigner
category=Vector
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

